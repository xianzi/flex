package
{
	import com.google.zxing.BarcodeFormat;
	import com.google.zxing.BinaryBitmap;
	import com.google.zxing.BufferedImageLuminanceSource;
	import com.google.zxing.DecodeHintType;
	import com.google.zxing.Result;
	import com.google.zxing.client.result.ParsedResult;
	import com.google.zxing.client.result.ResultParser;
	import com.google.zxing.common.GlobalHistogramBinarizer;
	import com.google.zxing.common.flexdatatypes.HashTable;
	import com.google.zxing.qrcode.QRCodeReader;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.media.Camera;
	import flash.media.Video;
	
	public class webcamqrcode extends Sprite
	{
		private var camera:Camera;
		private var video:Video;
		private var bitmap:Bitmap; 
		private var bitmapData:BitmapData;
		private var _qrCodeReader:QRCodeReader = new QRCodeReader();
		
		private var v_width:int = 640, v_height:int = 480;
		
		public function webcamqrcode()
		{
			ExternalInterface.addCallback("start", onstart);
			ExternalInterface.addCallback("stop", onstop);
			
			stage.scaleMode = StageScaleMode.NO_BORDER; //NO_SCALE;
			video = new Video(v_width, v_height);
			
			ExternalInterface.call("oninit");
		}
		
		private function onstart():void
		{
			this.graphics.clear();
			
			camera = Camera.getCamera();
			if(camera == null)
			{
				return;
			}
			camera.setQuality(0, 100);
			camera.setMode(v_width, v_height, 30, true );
			video.attachCamera( camera );
			this.addChild(video);
			
			video.x = stage.stageWidth / 2 - video.width / 2;
			video.y = stage.stageHeight / 2 - video.height / 2;
			
			bitmap = new Bitmap();
			bitmapData = new BitmapData(video.width, video.height, true, 0);
			
			this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onstop():void
		{
			video.attachCamera(null);
			this.removeChild(video);
			this.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(e:Event):void
		{
			bitmapData.draw(video);
			
			//_bitmapData转换到BufferedImageLuminanceSource。
			var bufferedImageLum:BufferedImageLuminanceSource = new BufferedImageLuminanceSource( bitmapData );
			var binaryBmp:BinaryBitmap = new BinaryBitmap( new GlobalHistogramBinarizer( bufferedImageLum ) );
			
			var hashTable:HashTable = new HashTable();
			hashTable.Add( DecodeHintType.POSSIBLE_FORMATS, BarcodeFormat.QR_CODE );
			
			try
			{
				// 解码
				var result:Result = _qrCodeReader.decode( binaryBmp, hashTable );
				if(result != null)
				{
					var parsedResult:ParsedResult = ResultParser.parseResult( result );
					var _qrCodeData:String = parsedResult.getDisplayResult();
					
					trace(_qrCodeData);
					ExternalInterface.call("onQrDecodeComplete", _qrCodeData);
				}
			}
			catch(err:Error)
			{
				trace(err);
			}
		}
	}
}