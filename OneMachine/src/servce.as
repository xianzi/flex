package
{
	import air.update.ApplicationUpdaterUI;
	
	import flash.desktop.NativeApplication;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.utils.Timer;
	
	import zhixin.flex.SocketClient;
	import zhixin.flex.commons;
	import zhixin.flex.downLoad;
	
	/**
	 * 客户端后台通讯服务
	 */
	public class servce
	{
		/**
		 * 
		 */
		private var socket:SocketClient;
		
		private var timer1:Timer = new Timer(10*60*1000);
		
		private var stage:Stage = null;
		
		public function servce(ipaddress:String, port:Number, stage:Stage)
		{
			this.stage = stage;
			timer1.addEventListener(TimerEvent.TIMER,function(e:TimerEvent):void{
				if(!socket.connected) init(ipaddress, port);
			});
			init(ipaddress, port);
		}
		private function init(ipaddress:String, port:Number):void{
			socket = new SocketClient(ipaddress, port);
			socket.addEventListener(IOErrorEvent.IO_ERROR,function(e:IOErrorEvent):void{
				if(!socket.connected) timer1.start();
			});
			socket.addEventListener(ProgressEvent.SOCKET_DATA,function(e:ProgressEvent):void{
				var msg:String = socket.message;
				fiterData(msg);
			});
			socket.addEventListener(Event.CLOSE,function(e:Event):void{
				timer1.start();
			});
			socket.addEventListener(Event.CONNECT,function(e:Event):void{
				timer1.stop();
			});
			socket.connection();
		}
		/**
		 * 过滤数据用户后台处理
		 */
		private function fiterData(msg:String):void{
			if(msg == null || msg == "" || msg.indexOf("\t") == -1) return;
			var items:Array = msg.split(/\t/);
			
			switch(items[0]){
				case "Alert": commons.alert(items[1]);break;
				case "Quit":
					NativeApplication.nativeApplication.exit();
					break;
				case "DownLoad":
					var down:downLoad = new downLoad();
					down.addEventListener(IOErrorEvent.IO_ERROR,function(error:IOErrorEvent):void{
						trace(error.toString());
					});
					down.down(items[1],items[2]);
					break;
				case "delete":
					var file:File = new File(items[1]);
					file.addEventListener(IOErrorEvent.IO_ERROR,function(error:IOErrorEvent):void{
						trace(error.toString());
					});
					if(file.exists) file.deleteFile();
					break;
				case "FullScreen":
					stage.displayState = StageDisplayState.NORMAL;
					break;
				case "Version":
					var appUpdater:ApplicationUpdaterUI = new ApplicationUpdaterUI();
					socket.sendMsg(appUpdater.currentVersion);
					break;
				case "ShutDown":
					commons.exeSystem("shutdown -f -r -t 0");
					break;
				case "ScreenCapturer":
					var filename:String = commons.now("YYYY-MM-DD H-NN-SS");
					commons.ScreenCapturer2File(stage,"C:\\Windows\\Temp\\" + filename + ".jpg");
					commons.upload("C:\\Windows\\Temp\\" + filename + ".jpg","http://www.hsh114.com/WebService/qx.php?action=sendHeartBeat&sn="+ filename);
					break;
			}
		}
	}
}